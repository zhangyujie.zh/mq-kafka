/**
 * *****************************************************
 * Copyright (C) 2019 bytedance.com. All Rights Reserved
 * This file is part of bytedance EA project.
 * Unauthorized copy of this file, via any medium is strictly prohibited.
 * Proprietary and Confidential.
 * ****************************************************
 **/
package com.bytedance.mq.mqkafka;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * @author 张玉杰 <zhangyujie.zh@bytedance.com>
 * @date 04/11/2019
 **/
@Component
@Slf4j
public class KafkaConsumer {

    private static final String DEFAULT_TOPIC = "";
    private static final String DEFAULT_LISTENER_ID = "DEFAULT";

    /**
     * 手动提交处理消费者监听数据和业务处理逻辑
     *
     * @param records        消费者监听到的kafka内容
     * @param acknowledgment 提交
     */
    @KafkaListener(id = DEFAULT_LISTENER_ID, topics = {DEFAULT_TOPIC})
    public void listen(ConsumerRecord<String, String> records, Acknowledgment acknowledgment) {
        Optional value = Optional.ofNullable(records.value());
        if (value.isPresent()) {
            String msg = value.get().toString();
            log.debug("获取消息主题为: [{}], 消息分区为: [{}], 分区的偏移量为: [{}], 时间戳为: [{}], 消息体为: [{}]",
                    records.topic(), records.partition(), records.offset(), records.timestamp(), msg);
            /*业务逻辑*/
            //.
        } else {
            log.error("数据无效！接收的记录为: [{}]", records.toString());
            /*异常处理逻辑*/
        }
        /*修改配置ENABLE_AUTO_COMMIT_CONFIG为FALSE，手动提交偏移量*/
        acknowledgment.acknowledge();
    }
}