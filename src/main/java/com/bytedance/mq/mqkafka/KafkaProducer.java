/**
 * *****************************************************
 * Copyright (C) 2019 bytedance.com. All Rights Reserved
 * This file is part of bytedance EA project.
 * Unauthorized copy of this file, via any medium is strictly prohibited.
 * Proprietary and Confidential.
 * ****************************************************
 **/
package com.bytedance.mq.mqkafka;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;

import javax.annotation.Resource;
import java.util.concurrent.ExecutionException;

/**
 * @author 张玉杰 <zhangyujie.zh@bytedance.com>
 * @date 04/11/2019
 **/
@Component
@Slf4j
public class KafkaProducer {

    private static final String DEFAULT_TOPIC = "";

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;

    /**
     * 发送Message数据到指定topic，并按照key值作为分区标准
     *
     * @param topic   kafka主题
     * @param key     消息进入分区依据
     * @param message 待发送消息体
     * @throws InterruptedException 中断异常
     */
    public <T> void send(String topic, String key, T message) throws InterruptedException {
        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topic, key, message.toString());
        try {
            SendResult<String, String> result = future.get();
            log.info("发送数据成功, 数据为: [{}]", result.toString());
        } catch (ExecutionException e) {
            log.error("发送数据ExecutionException异常, 数据为: [{}]", message.toString(), e);
        }
    }

    /**
     * 发送Message数据到默认topic
     *
     * @param message 待发送消息对象
     * @throws InterruptedException 中断异常
     */
    public <T> void send(T message) throws InterruptedException {
        this.send(DEFAULT_TOPIC, null, message);
    }

    /**
     * 发送Message数据到指定topic
     *
     * @param topic   kafka主题
     * @param message 待发送消息体
     * @throws InterruptedException 中断异常
     */
    public <T> void send(String topic, T message) throws InterruptedException {
        this.send(topic, null, message);
    }
}