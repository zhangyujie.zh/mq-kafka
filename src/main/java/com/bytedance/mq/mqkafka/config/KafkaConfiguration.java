/**
 * *****************************************************
 * Copyright (C) 2019 bytedance.com. All Rights Reserved
 * This file is part of bytedance EA project.
 * Unauthorized copy of this file, via any medium is strictly prohibited.
 * Proprietary and Confidential.
 * ****************************************************
 **/
package com.bytedance.mq.mqkafka.config;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;

/**
 * @author 张玉杰 <zhangyujie.zh@bytedance.com>
 * @date 04/11/2019
 **/
@EnableKafka
@Configuration
public class KafkaConfiguration {

}